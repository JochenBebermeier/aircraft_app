require 'test_helper'

class RouteStateAssociationsControllerTest < ActionController::TestCase
  setup do
    @route_state_association = route_state_associations(:one)
  end


  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create route_state_association" do
    assert_difference('RouteStateAssociation.count') do
      post :create, route_state_association: { demand: @route_state_association.demand, lambda: @route_state_association.lambda, route_id: @route_state_association.route_id, state_id: @route_state_association.state_id }
    end

    assert_redirected_to route_state_association_path(assigns(:route_state_association))
  end

  test "should show route_state_association" do
    get :show, id: @route_state_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @route_state_association
    assert_response :success
  end

  test "should update route_state_association" do
    patch :update, id: @route_state_association, route_state_association: { demand: @route_state_association.demand, lambda: @route_state_association.lambda, route_id: @route_state_association.route_id, state_id: @route_state_association.state_id }
    assert_redirected_to route_state_association_path(assigns(:route_state_association))
  end

  test "should destroy route_state_association" do
    assert_difference('RouteStateAssociation.count', -1) do
      delete :destroy, id: @route_state_association
    end

    assert_redirected_to route_state_associations_path
  end
end
