require 'test_helper'

class RoutePlaneAssociationsControllerTest < ActionController::TestCase
  setup do
    @route_plane_association = route_plane_associations(:one)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create route_plane_association" do
    assert_difference('RoutePlaneAssociation.count') do
      post :create, route_plane_association: { capacity: @route_plane_association.capacity, costs: @route_plane_association.costs, plane_id: @route_plane_association.plane_id, route_id: @route_plane_association.route_id }
    end

    assert_redirected_to route_plane_association_path(assigns(:route_plane_association))
  end

  test "should show route_plane_association" do
    get :show, id: @route_plane_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @route_plane_association
    assert_response :success
  end

  test "should update route_plane_association" do
    patch :update, id: @route_plane_association, route_plane_association: { capacity: @route_plane_association.capacity, costs: @route_plane_association.costs, plane_id: @route_plane_association.plane_id, route_id: @route_plane_association.route_id }
    assert_redirected_to route_plane_association_path(assigns(:route_plane_association))
  end

  test "should destroy route_plane_association" do
    assert_difference('RoutePlaneAssociation.count', -1) do
      delete :destroy, id: @route_plane_association
    end

    assert_redirected_to route_plane_associations_path
  end
end
