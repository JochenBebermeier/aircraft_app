class AddPassengersBumpedToRouteStateAssociation < ActiveRecord::Migration
  def change
    add_column :route_state_associations, :passengers_bumped, :float
  end
end
