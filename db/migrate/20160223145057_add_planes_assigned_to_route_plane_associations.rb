class AddPlanesAssignedToRoutePlaneAssociations < ActiveRecord::Migration
  def change
    add_column :route_plane_associations, :planes_assigned, :float
  end
end
