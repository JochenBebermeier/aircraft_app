class AddPassengersCarriedToRouteStateAssociation < ActiveRecord::Migration
  def change
    add_column :route_state_associations, :passengers_carried, :float
  end
end
