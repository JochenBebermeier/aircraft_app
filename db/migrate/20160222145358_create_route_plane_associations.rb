class CreateRoutePlaneAssociations < ActiveRecord::Migration
  def change
    create_table :route_plane_associations do |t|
      t.integer :route_id
      t.integer :plane_id
      t.integer :capacity
      t.float :costs

      t.timestamps null: false
    end
  end
end
