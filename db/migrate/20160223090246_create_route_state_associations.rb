class CreateRouteStateAssociations < ActiveRecord::Migration
  def change
    create_table :route_state_associations do |t|
      t.integer :route_id
      t.integer :state_id
      t.integer :demand
      t.float :lambda

      t.timestamps null: false
    end
  end
end
