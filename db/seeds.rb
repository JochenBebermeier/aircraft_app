#encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(name:  "Jochen Bebermeier",
             email: "jb@ruby.de",
             gams:  "C:\\GAMS\\win64\\24.5\\gams",
             password:              "password",
             password_confirmation: "password",
             admin: true)


# Transport model


S1 = Site.create!(name: "Frankfurt(M)",
                  codename: "FRA")
S2 = Site.create!(name: "Berlin-Tegel",
                  codename: "TLX")
S3 = Site.create!(name: "München",
                  codename: "MUC")
S4 = Site.create!(name: "Bremen",
                  codename: "BRE")
S5 = Site.create!(name: "Cottbus",
                  codename: "CBU")
S6 = Site.create!(name: "Düsseldorf",
                  codename: "DUS")
S7 = Site.create!(name: "Hamburg",
                  codename: "HAM")
S8 = Site.create!(name: "Stuttgart",
                  codename: "STR")


(10..99).each do |n|
  name = "Beispielort-#{n}"
  codename = "C#{n}"
  Site.create!(name: name,
               codename: codename)
end


SuSi1 = Supplysite.create!(site_id: S1.id, supply_quantity: 20)

SuSi2 = Supplysite.create!(site_id: S2.id, supply_quantity: 25)

SuSi3 = Supplysite.create!(site_id: S3.id, supply_quantity: 21)

DeSi1 = Demandsite.create!(site_id: S4.id, demand_quantity: 15)

DeSi2 = Demandsite.create!(site_id: S5.id, demand_quantity: 17)

DeSi3 = Demandsite.create!(site_id: S6.id, demand_quantity: 22)

DeSi4 = Demandsite.create!(site_id: S7.id, demand_quantity: 12)

TraLi1 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi1.id, unit_cost: 6, transport_quantity: 0.0)
TraLi2 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi2.id, unit_cost: 2, transport_quantity: 0.0)
TraLi3 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi3.id, unit_cost: 6, transport_quantity: 0.0)
TraLi4 = Translink.create!(supplysite_id: SuSi1.id, demandsite_id: DeSi4.id, unit_cost: 7, transport_quantity: 0.0)
TraLi5 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi1.id, unit_cost: 4, transport_quantity: 0.0)
TraLi6 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi2.id, unit_cost: 9, transport_quantity: 0.0)
TraLi7 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi3.id, unit_cost: 5, transport_quantity: 0.0)
TraLi8 = Translink.create!(supplysite_id: SuSi2.id, demandsite_id: DeSi4.id, unit_cost: 3, transport_quantity: 0.0)
TraLi9 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi1.id, unit_cost: 8, transport_quantity: 0.0)
TraLi10 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi2.id, unit_cost: 8, transport_quantity: 0.0)
TraLi11 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi3.id, unit_cost: 1, transport_quantity: 0.0)
TraLi12 = Translink.create!(supplysite_id: SuSi3.id, demandsite_id: DeSi4.id, unit_cost: 6, transport_quantity: 0.0)

#MLCLSP

Prod1 = Product.create!(name: "P1", setup_time: 20, processing_time: 1,
                        setup_cost: 200, holding_cost: 40,
                        initial_inventory: 50, lead_time_periods: 1, machine_id: 1)

Prod2 = Product.create!(name: "P2", setup_time: 30, processing_time: 1,
                        setup_cost: 500, holding_cost: 35,
                        initial_inventory: 120, lead_time_periods: 1, machine_id: 2)

Prod3 = Product.create!(name: "P3", setup_time: 10, processing_time: 1,
                        setup_cost: 300, holding_cost: 15,
                        initial_inventory: 100, lead_time_periods: 1, machine_id: 2)

Prod4 = Product.create!(name: "P4", setup_time: 20, processing_time: 1,
                        setup_cost: 1000, holding_cost: 4,
                        initial_inventory: 110, lead_time_periods: 1, machine_id: 2)

Prod5 = Product.create!(name: "P5", setup_time: 10, processing_time: 1,
                        setup_cost: 300, holding_cost: 3,
                        initial_inventory: 80, lead_time_periods: 1, machine_id: 1)


(1..4).each do |n|
  name = "t#{n}"
  Period.create!(name: name)
end

(1..2).each do |n|
  name = "m#{n}"
  Machine.create!(name: name, overtime_cost: 200)
end

(1..2).each do |m|
  (1..4).each do |n|
    MachinePeriodAssociation.create!(machine_id: m, period_id: n, capacity: 150)
  end
end

P1t1 = ProductPeriodAssociation.create!(product_id: 1, period_id: 1, demand: 10)
P1t2 = ProductPeriodAssociation.create!(product_id: 1, period_id: 2, demand: 20)
P1t3 = ProductPeriodAssociation.create!(product_id: 1, period_id: 3, demand: 30)
P1t4 = ProductPeriodAssociation.create!(product_id: 1, period_id: 4, demand: 80)

P2t1 = ProductPeriodAssociation.create!(product_id: 2, period_id: 1, demand: 20)
P2t2 = ProductPeriodAssociation.create!(product_id: 2, period_id: 2, demand: 80)
P2t3 = ProductPeriodAssociation.create!(product_id: 2, period_id: 3, demand: 50)
P2t4 = ProductPeriodAssociation.create!(product_id: 2, period_id: 4, demand: 90)

P3t1 = ProductPeriodAssociation.create!(product_id: 3, period_id: 1, demand: 0)
P3t2 = ProductPeriodAssociation.create!(product_id: 3, period_id: 2, demand: 0)
P3t3 = ProductPeriodAssociation.create!(product_id: 3, period_id: 3, demand: 30)
P3t4 = ProductPeriodAssociation.create!(product_id: 3, period_id: 4, demand: 0)

P4t1 = ProductPeriodAssociation.create!(product_id: 4, period_id: 1, demand: 0)
P4t2 = ProductPeriodAssociation.create!(product_id: 4, period_id: 2, demand: 0)
P4t3 = ProductPeriodAssociation.create!(product_id: 4, period_id: 3, demand: 0)
P4t4 = ProductPeriodAssociation.create!(product_id: 4, period_id: 4, demand: 80)

P5t1 = ProductPeriodAssociation.create!(product_id: 5, period_id: 1, demand: 0)
P5t2 = ProductPeriodAssociation.create!(product_id: 5, period_id: 2, demand: 0)
P5t3 = ProductPeriodAssociation.create!(product_id: 5, period_id: 3, demand: 0)
P5t4 = ProductPeriodAssociation.create!(product_id: 5, period_id: 4, demand: 0)

P3P1 = ProductProductAssociation.create!(predecessor_id: 3, successor_id:1, coefficient: 1 )
P3P2 = ProductProductAssociation.create!(predecessor_id: 3, successor_id:2, coefficient: 1 )
P4P1 = ProductProductAssociation.create!(predecessor_id: 4, successor_id:1, coefficient: 1 )
P5P3 = ProductProductAssociation.create!(predecessor_id: 5, successor_id:3, coefficient: 1 )
P5P4 = ProductProductAssociation.create!(predecessor_id: 5, successor_id:4, coefficient: 1 )

#Aircraft Model

Rou1 = Route.create!(name: "R1", revenue_loss: 13.0)
Rou2 = Route.create!(name: "R2", revenue_loss: 13.0)
Rou3 = Route.create!(name: "R3", revenue_loss: 7.0)
Rou4 = Route.create!(name: "R4", revenue_loss: 7.0)
Rou5 = Route.create!(name: "R5", revenue_loss: 1.0)

PlaA = Plane.create!(name: "TypeA", plane_availability: 10.0)
PlaB = Plane.create!(name: "TypeB", plane_availability: 19.0)
PlaC = Plane.create!(name: "TypeC", plane_availability: 25.0)
PlaD = Plane.create!(name: "TypeD", plane_availability: 15.0)

Sta1 = State.create!(name: "Scenario1")
Sta2 = State.create!(name: "Scenario2")
Sta3 = State.create!(name: "Scenario3")
Sta4 = State.create!(name: "Scenario4")
Sta5 = State.create!(name: "Scenario5")

R1PA = RoutePlaneAssociation.create!(route_id: 1, plane_id: 1, capacity: 16.0, costs: 18.0)
R1PD = RoutePlaneAssociation.create!(route_id: 1, plane_id: 2, capacity: 9.0, costs: 17.0)

R2PA = RoutePlaneAssociation.create!(route_id: 2, plane_id: 1, capacity: 15, costs: 21)
R2PB = RoutePlaneAssociation.create!(route_id: 2, plane_id: 2, capacity: 10, costs: 15)
R2PC = RoutePlaneAssociation.create!(route_id: 2, plane_id: 3, capacity: 5, costs: 10)
R2PD = RoutePlaneAssociation.create!(route_id: 2, plane_id: 4, capacity: 11, costs: 16)

R3PA = RoutePlaneAssociation.create!(route_id: 3, plane_id: 1, capacity: 28, costs: 18)
R3PB = RoutePlaneAssociation.create!(route_id: 3, plane_id: 2, capacity: 14, costs: 16)
R3PD = RoutePlaneAssociation.create!(route_id: 3, plane_id: 4, capacity: 22, costs: 17)

R4PA = RoutePlaneAssociation.create!(route_id: 4, plane_id: 1, capacity: 23, costs: 16)
R4PB = RoutePlaneAssociation.create!(route_id: 4, plane_id: 2, capacity: 15, costs: 14)
R4PC = RoutePlaneAssociation.create!(route_id: 4, plane_id: 3, capacity: 7, costs: 9)
R4PD = RoutePlaneAssociation.create!(route_id: 4, plane_id: 4, capacity: 17, costs: 15)

R5PA = RoutePlaneAssociation.create!(route_id: 5, plane_id: 1, capacity: 81, costs: 10)
R5PB = RoutePlaneAssociation.create!(route_id: 5, plane_id: 2, capacity: 57, costs: 9)
R5PC = RoutePlaneAssociation.create!(route_id: 5, plane_id: 3, capacity: 29, costs: 6)
R5PD = RoutePlaneAssociation.create!(route_id: 5, plane_id: 4, capacity: 55, costs: 10)

R1S1 = RouteStateAssociation.create!(route_id: 1, state_id: 1, demand: 200, lambda: 0.2)
R1S2 = RouteStateAssociation.create!(route_id: 1, state_id: 2, demand: 220, lambda: 0.05)
R1S3 = RouteStateAssociation.create!(route_id: 1, state_id: 3, demand: 250, lambda: 0.35)
R1S4 = RouteStateAssociation.create!(route_id: 1, state_id: 4, demand: 270, lambda: 0.2)
R1S5 = RouteStateAssociation.create!(route_id: 1, state_id: 5, demand: 300, lambda: 0.2)

R2S1 = RouteStateAssociation.create!(route_id: 2, state_id: 1, demand: 50, lambda: 0.3)
R2S2 = RouteStateAssociation.create!(route_id: 2, state_id: 2, demand: 150, lambda: 0.7)

R3S1 = RouteStateAssociation.create!(route_id: 3, state_id: 1, demand: 140, lambda: 0.1)
R3S2 = RouteStateAssociation.create!(route_id: 3, state_id: 2, demand: 160, lambda: 0.20)
R3S3 = RouteStateAssociation.create!(route_id: 3, state_id: 3, demand: 180, lambda: 0.4)
R3S4 = RouteStateAssociation.create!(route_id: 3, state_id: 4, demand: 200, lambda: 0.2)
R3S5 = RouteStateAssociation.create!(route_id: 3, state_id: 5, demand: 220, lambda: 0.1)

R4S1 = RouteStateAssociation.create!(route_id: 4, state_id: 1, demand: 10 , lambda: 0.2)
R4S2 = RouteStateAssociation.create!(route_id: 4, state_id: 2, demand: 50, lambda: 0.2)
R4S3 = RouteStateAssociation.create!(route_id: 4, state_id: 3, demand: 80, lambda: 0.3)
R4S4 = RouteStateAssociation.create!(route_id: 4, state_id: 4, demand: 100, lambda: 0.2)
R4S5 = RouteStateAssociation.create!(route_id: 4, state_id: 5, demand: 340, lambda: 0.1)

R5S1 = RouteStateAssociation.create!(route_id: 5, state_id: 1, demand: 580, lambda: 0.1)
R5S2 = RouteStateAssociation.create!(route_id: 5, state_id: 2, demand: 600, lambda: 0.8)
R5S3 = RouteStateAssociation.create!(route_id: 5, state_id: 3, demand: 620, lambda: 0.1)


