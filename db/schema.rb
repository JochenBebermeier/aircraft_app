# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160224120103) do

  create_table "demandsites", force: :cascade do |t|
    t.integer  "site_id"
    t.float    "demand_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "machine_period_associations", force: :cascade do |t|
    t.integer  "machine_id"
    t.integer  "period_id"
    t.float    "capacity"
    t.float    "overtime"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "machines", force: :cascade do |t|
    t.string   "name"
    t.float    "overtime_cost"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "microposts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "microposts", ["user_id", "created_at"], name: "index_microposts_on_user_id_and_created_at"
  add_index "microposts", ["user_id"], name: "index_microposts_on_user_id"

  create_table "periods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "planes", force: :cascade do |t|
    t.string   "name"
    t.integer  "plane_availability"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "product_period_associations", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "period_id"
    t.float    "demand"
    t.float    "production_quantity"
    t.float    "inventory_level"
    t.integer  "setup_variable"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "product_product_associations", force: :cascade do |t|
    t.integer  "predecessor_id"
    t.integer  "successor_id"
    t.float    "coefficient"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.float    "setup_time"
    t.float    "processing_time"
    t.float    "setup_cost"
    t.float    "holding_cost"
    t.float    "initial_inventory"
    t.integer  "lead_time_periods"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "machine_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id"

  create_table "route_plane_associations", force: :cascade do |t|
    t.integer  "route_id"
    t.integer  "plane_id"
    t.integer  "capacity"
    t.float    "costs"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.float    "planes_assigned"
  end

  create_table "route_state_associations", force: :cascade do |t|
    t.integer  "route_id"
    t.integer  "state_id"
    t.integer  "demand"
    t.float    "lambda"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.float    "passengers_carried"
    t.float    "passengers_bumped"
  end

  create_table "routes", force: :cascade do |t|
    t.string   "name"
    t.float    "revenue_loss"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "sites", force: :cascade do |t|
    t.string   "name"
    t.string   "codename",   limit: 3
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: :cascade do |t|
    t.integer  "states_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supplysites", force: :cascade do |t|
    t.integer  "site_id"
    t.float    "supply_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "translinks", force: :cascade do |t|
    t.integer  "supplysite_id"
    t.integer  "demandsite_id"
    t.float    "unit_cost"
    t.float    "transport_quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "gams"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
