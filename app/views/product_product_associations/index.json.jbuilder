json.array!(@product_product_associations) do |product_product_association|
  json.extract! product_product_association, :id, :predecessor_id, :successor_id, :coefficient
  json.url product_product_association_url(product_product_association, format: :json)
end
