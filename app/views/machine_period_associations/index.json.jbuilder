json.array!(@machine_period_associations) do |machine_period_association|
  json.extract! machine_period_association, :id, :machine_id, :period_id, :capacity, :overtime
  json.url machine_period_association_url(machine_period_association, format: :json)
end
