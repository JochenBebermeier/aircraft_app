json.array!(@route_plane_associations) do |route_plane_association|
  json.extract! route_plane_association, :id, :route_id, :plane_id, :capacity, :costs
  json.url route_plane_association_url(route_plane_association, format: :json)
end
