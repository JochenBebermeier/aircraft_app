json.array!(@routes) do |route|
  json.extract! route, :id, :name, :revenue_loss
  json.url route_url(route, format: :json)
end
