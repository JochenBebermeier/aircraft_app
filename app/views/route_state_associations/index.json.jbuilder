json.array!(@route_state_associations) do |route_state_association|
  json.extract! route_state_association, :id, :route_id, :state_id, :demand, :lambda
  json.url route_state_association_url(route_state_association, format: :json)
end
