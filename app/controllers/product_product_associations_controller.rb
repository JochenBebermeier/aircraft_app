class ProductProductAssociationsController < ApplicationController
  before_action :set_product_product_association, only: [:show, :edit, :update, :destroy]

  # GET /product_product_associations
  # GET /product_product_associations.json
  def index
    @product_product_associations = ProductProductAssociation.all
  end

  # GET /product_product_associations/1
  # GET /product_product_associations/1.json
  def show
  end

  # GET /product_product_associations/new
  def new
    @product_product_association = ProductProductAssociation.new
  end

  # GET /product_product_associations/1/edit
  def edit
  end

  # POST /product_product_associations
  # POST /product_product_associations.json
  def create
    @product_product_association = ProductProductAssociation.new(product_product_association_params)

    respond_to do |format|
      if @product_product_association.save
        format.html { redirect_to @product_product_association, notice: 'Product product association was successfully created.' }
        format.json { render :show, status: :created, location: @product_product_association }
      else
        format.html { render :new }
        format.json { render json: @product_product_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_product_associations/1
  # PATCH/PUT /product_product_associations/1.json
  def update
    respond_to do |format|
      if @product_product_association.update(product_product_association_params)
        format.html { redirect_to @product_product_association, notice: 'Product product association was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_product_association }
      else
        format.html { render :edit }
        format.json { render json: @product_product_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_product_associations/1
  # DELETE /product_product_associations/1.json
  def destroy
    @product_product_association.destroy
    respond_to do |format|
      format.html { redirect_to product_product_associations_url, notice: 'Product product association was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_product_association
      @product_product_association = ProductProductAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_product_association_params
      params.require(:product_product_association).permit(:predecessor_id, :successor_id, :coefficient)
    end
end
