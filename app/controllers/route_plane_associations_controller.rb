class RoutePlaneAssociationsController < ApplicationController
  before_action :set_route_plane_association, only: [:show, :edit, :update, :destroy]

  # GET /route_plane_associations
  # GET /route_plane_associations.json
  def index
    @route_plane_associations = RoutePlaneAssociation.all
  end

  # GET /route_plane_associations/1
  # GET /route_plane_associations/1.json
  def show
  end

  # GET /route_plane_associations/new
  def new
    @route_plane_association = RoutePlaneAssociation.new
  end

  # GET /route_plane_associations/1/edit
  def edit
  end

  # POST /route_plane_associations
  # POST /route_plane_associations.json
  def create
    @route_plane_association = RoutePlaneAssociation.new(route_plane_association_params)

    respond_to do |format|
      if @route_plane_association.save
        format.html { redirect_to @route_plane_association, notice: 'Route plane association was successfully created.' }
        format.json { render :show, status: :created, location: @route_plane_association }
      else
        format.html { render :new }
        format.json { render json: @route_plane_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /route_plane_associations/1
  # PATCH/PUT /route_plane_associations/1.json
  def update
    respond_to do |format|
      if @route_plane_association.update(route_plane_association_params)
        format.html { redirect_to @route_plane_association, notice: 'Route plane association was successfully updated.' }
        format.json { render :show, status: :ok, location: @route_plane_association }
      else
        format.html { render :edit }
        format.json { render json: @route_plane_association.errors, status: :unprocessable_entity }
      end
    end
  end

  #DELETE /route_state_associations/1
  #DELETE /route_state_associations/1.json
  def destroy
    @route_plane_association.destroy
    respond_to do |format|
      format.html { redirect_to route_plane_associations_url, notice: 'Route plane association was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

=begin
  def delete_old_plan

      if File.exist?("Flightplan.txt")
        File.delete("Flightplan.txt")
      end

      if File.exist?("passengers.txt")
        File.delete("passengers.txt")
      end


      if File.exist?("total_costs.txt")
        File.delete("total_costs.txt")
      end

      @route_plane_association = RoutePlaneAssociation.all
      @route_plane_association.each { |rou_pla|
        rou_pla.planes_assigned=nil
        rou_pla.save
      }

      @route_state_association = RouteStateAssociation.all
      @route_state_association.each { |rou_sta|
        rou_sta.passengers_carried=nil
        rou_sta.passengers_bumped=nil
        rou_sta.save
      }

      @objective_function_value=nil

      render :template => "route_plane_associations/index"
=end

  def optimize

    if File.exist?("Flightplan.txt")
      File.delete("Flightplan.txt")
    end

    if File.exist?("passengers.txt")
      File.delete("passengers.txt")
    end


    if File.exist?("total_costs.txt")
      File.delete("total_costs.txt")
    end


    if File.exist?("results.xlsx")
      File.rename("results.xlsx", Time.now.strftime('%Y-%m-%d_%H-%M-%S')+"_previous_results.xlsx")
    end

    @route_plane_association = RoutePlaneAssociation.all
    @route_plane_association.each { |rou_pla|
      rou_pla.planes_assigned=nil
      rou_pla.save
    }

    @route_state_association = RouteStateAssociation.all
    @route_state_association.each { |rou_sta|
      rou_sta.passengers_carried=nil
      rou_sta.passengers_bumped=nil
      rou_sta.save
    }

    @objective_function_value=nil
    

    if File.exist?("Aircraft_Allocation_Input.inc")
      File.delete("Aircraft_Allocation_Input.inc")
    end

    f=File.new("Aircraft_Allocation_Input.inc", "w")
    printf(f, "set i / \n")
    @planes = Plane.all
    @planes.each { |plane| printf(f, plane.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "set j / \n")
    @routes = Route.all
    @routes.each { |route| printf(f, route.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "set h / \n")
    @states = State.all
    @states.each { |state| printf(f, state.name + "\n") }
    printf(f, "/;\n\n")

    printf(f, "set ijr / \n")
    @route_plane_association = RoutePlaneAssociation.all
    @route_plane_association.each { |route_plane_association| printf(f, "ijr" + route_plane_association.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "set jhr / \n")
    @route_state_association = RouteStateAssociation.all
    @route_state_association.each { |route_state_association| printf(f, "jhr" + route_state_association.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "IJ(i,j)=no;\n")
    printf(f, "JH(j,h)=no;\n\n")
    @route_plane_association = RoutePlaneAssociation.all
    @route_plane_association.each { |rou_pla| printf(f, "IJ('" + rou_pla.plane.name+"','" + rou_pla.route.name+"')=yes;\n") }

    @route_state_association = RouteStateAssociation.all
    @route_state_association.each { |rou_sta| printf(f, "JH('" + rou_sta.route.name+"','" + rou_sta.state.name+"')=yes;\n") }

    printf(f, "IJrIJ(ijr,i,j)=no;\n")
    printf(f, "JHrJH(jhr,j,h)=no;\n\n")
    @route_plane_association.each { |rou_pla| printf(f, "IJrIJ('ijr" + rou_pla.id.to_s + "','" + rou_pla.plane.name + "','" + rou_pla.route.name + "')=yes;\n") }
    printf(f, "\n\n")

    @route_state_association.each { |rou_sta| printf(f, "JHrJH('jhr" + rou_sta.id.to_s + "','" + rou_sta.route.name + "','" + rou_sta.state.name + "')=yes;\n") }
    printf(f, "\n\n")


    @routes.each { |route| printf(f, "k('" + route.name + "')= "+ route.revenue_loss.to_s + ";\n") }

    @planes.each { |plane| printf(f, "aa('" + plane.name + "')= "+ plane.plane_availability.to_s + ";\n") }

    @route_plane_association = RoutePlaneAssociation.all
    @route_plane_association.each { |rou_pla|
      printf(f, "c('" + rou_pla.plane.name+"','" + rou_pla.route.name+"')= "+ rou_pla.costs.to_s + ";\n")
      printf(f, "p('" + rou_pla.plane.name+"','" + rou_pla.route.name+"')= "+ rou_pla.capacity.to_s + ";\n")
    }

    @route_state_association = RouteStateAssociation.all
    @route_state_association.each { |rou_sta|
      printf(f, "dd('" + rou_sta.route.name+"','" + rou_sta.state.name+"')= "+ rou_sta.demand.to_s + ";\n")
      printf(f, "lambda('" + rou_sta.route.name+"','" + rou_sta.state.name+"')= "+ rou_sta.lambda.to_s + ";\n")
    }

    f.close

    @gams_path = current_user.gams
    system "#{@gams_path} aircraft_model"

    flash.now[:started] = "The calculation has been started!"

    render 'static_pages/aircraft_start'

  end



  def read_optimization_results


    if (File.exist?("Flightplan.txt") and File.exists?("passengers.txt"))

      fi=File.open("Flightplan.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "ijr "
        sa3=sa[3].delete " \n"
        route_plane_association=RoutePlaneAssociation.find_by_id(sa0)
        route_plane_association.planes_assigned = sa3

        route_plane_association.save
      }
      fi.close

      fi=File.open("passengers.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "jhr "
        sa3=sa[3]
        sa4=sa[4].delete " \n"
        route_state_association=RouteStateAssociation.find_by_id(sa0)
        route_state_association.passengers_carried  = sa3
        route_state_association.passengers_bumped  = sa4
        route_state_association.save
      }
      fi.close

    else
      flash.now[:not_available] = "The solution has not been calculated yet!"
    end
    @route_plane_associations = RoutePlaneAssociation.all
    render :template => "route_plane_associations/index"

  end


  def read_and_show_ofv

    if File.exist?("total_costs.txt")
      fi=File.open("total_costs.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil
      flash.now[:not_available] = "Objective function value has not been calculated yet!"

    end

    @route_plane_associations = RoutePlaneAssociation.all
    render :template => "route_plane_associations/index"
  end




  private
  # Use callbacks to share common setup or constraints between actions.
  def set_route_plane_association
    @route_plane_association = RoutePlaneAssociation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def route_plane_association_params
    params.require(:route_plane_association).permit(:route_id, :plane_id, :capacity, :costs, :planes_assigned)
  end

end






