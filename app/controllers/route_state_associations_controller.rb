class RouteStateAssociationsController < ApplicationController
  before_action :set_route_state_association, only: [:show, :edit, :update, :destroy]

  # GET /route_state_associations
  # GET /route_state_associations.json
  def index
    @route_state_associations = RouteStateAssociation.all
  end

  # GET /route_state_associations/1
  # GET /route_state_associations/1.json
  def show
  end

  # GET /route_state_associations/new
  def new
    @route_state_association = RouteStateAssociation.new
  end

  # GET /route_state_associations/1/edit
  def edit
  end

  # POST /route_state_associations
  # POST /route_state_associations.json
  def create
    @route_state_association = RouteStateAssociation.new(route_state_association_params)

    respond_to do |format|
      if @route_state_association.save
        format.html { redirect_to @route_state_association, notice: 'Route state association was successfully created.' }
        format.json { render :show, status: :created, location: @route_state_association }
      else
        format.html { render :new }
        format.json { render json: @route_state_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /route_state_associations/1
  # PATCH/PUT /route_state_associations/1.json
  def update
    respond_to do |format|
      if @route_state_association.update(route_state_association_params)
        format.html { redirect_to @route_state_association, notice: 'Route state association was successfully updated.' }
        format.json { render :show, status: :ok, location: @route_state_association }
      else
        format.html { render :edit }
        format.json { render json: @route_state_association.errors, status: :unprocessable_entity }
      end
    end
  end

  def read_and_show_passengers


    if (File.exist?("passengers.txt"))

      fi=File.open("passengers.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "jhr "
        sa3=sa[3]
        sa4=sa[4].delete " \n"
        route_state_association=RouteStateAssociation.find_by_id(sa0)
        route_state_association.passengers_carried = sa3
        route_state_association.passengers_bumped = sa4

        route_state_association.save
      }
      fi.close

    else
      flash.now[:not_available] = "The solution has not been calculated yet!"
    end
    @route_state_associations = RouteStateAssociation.all
    render :template => "route_state_associations/index"

  end


  #DELETE /route_state_associations/1
  #DELETE /route_state_associations/1.json
  def destroy
    @route_state_association.destroy
    respond_to do |format|
      format.html { redirect_to route_state_associations_url, notice: 'Route state association was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_route_state_association
      @route_state_association = RouteStateAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def route_state_association_params
      params.require(:route_state_association).permit(:route_id, :state_id, :demand, :lambda)
    end
end
