class RouteStateAssociation < ActiveRecord::Base
  validates :route_id, presence: true
  validates :state_id, presence: true
  validates :demand, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :lambda, presence: true, :numericality => { :greater_than_or_equal_to => 0 }

  belongs_to :route
  belongs_to :state
end