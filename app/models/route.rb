class Route < ActiveRecord::Base
  validates :name, presence: true
  validates :revenue_loss, presence: true
  validates :revenue_loss, :numericality => { :greater_than_or_equal_to => 0 }

  has_many :planes, through: :route_plane_associations
  has_many :states, through: :route_state_associations
  has_many :route_plane_associations, foreign_key: "route_id", :dependent => :destroy
  has_many :route_state_associations, foreign_key: "route_id", :dependent => :destroy

end