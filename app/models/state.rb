class State< ActiveRecord::Base
  validates :name, presence: true

  has_many :routes, through: :route_state_associations
  has_many :route_plane_associations, foreign_key: "state_id", :dependent => :destroy
end