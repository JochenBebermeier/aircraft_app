class Plane< ActiveRecord::Base
  validates :name, presence: true
  validates :plane_availability, presence: true
  validates :plane_availability, :numericality => { :greater_than_or_equal_to => 0 }

  has_many :routes, through: :route_plane_associations
  has_many :route_plane_associations, foreign_key: "plane_id", :dependent => :destroy
end
