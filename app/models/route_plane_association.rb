class RoutePlaneAssociation < ActiveRecord::Base
  validates :route_id, presence: true
  validates :plane_id, presence: true
  validates :capacity, presence: true, :numericality => { :greater_than_or_equal_to => 0 }
  validates :costs, presence: true, :numericality => { :greater_than_or_equal_to => 0 }


  belongs_to :route
  belongs_to :plane
end