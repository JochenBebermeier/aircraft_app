class Machine < ActiveRecord::Base
  has_many :products
  has_many :periods, through: :machine_period_associations
  has_many :machine_period_associations, :dependent => :destroy
end
