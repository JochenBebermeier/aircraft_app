$Title Aircraft allocation under uncertain demand  (AIRCRAF,SEQ=8)



$Ontext



The objective of this model is to allocate aircrafts to routes to maximize

the expected profit when traffic demand is uncertain. Two different

formulations are used, the delta and the lambda formulation.





Dantzig, G B, Chapter 28. In Linear Programming and Extensions.

Princeton University Press, Princeton, New Jersey, 1963.



$Offtext



Set        i             aircraft types
           j             routes
           h             demand_states
           ijr           relationship-aircraft routes
           jhr           relationship routes demand states
           IJ(i,j)       aircraft-routes-association
           JH(j,h)       route-state-association
           IJrIJ(ijr,i,j)
           JHrJH(jhr,j,h)

Alias(h,hp);

Parameter  dd(j,h)     demand distribution on route j
           lambda(j,h) probability of demand state h on route j
           c(i,j)      costs per aircraft (1000s)
           p(i,j)      passenger capacity of aircraft i on route j
           aa(i)       aircraft availability
           k(j)        revenue lost (1000 per 100  bumped)
           ed(j)       expected demand
           gamma(j,h)  probability of exceeding demand increment h on route j
           deltb(j,h)  incremental passenger load in demand states;

$include "Aircraft_Allocation_Input.inc";

 ed(j)      = sum(h, lambda(j,h)*dd(j,h));
 gamma(j,h) = sum(hp$(ord(hp) ge ord(h)), lambda(j,hp));
 deltb(j,h) = (dd(j,h)-dd(j,h-1))$dd(j,h);

Display ed, gamma, deltb;

Integer Variable
         x(i,j)  number of aircraft type i assigned to route j;

Positive Variables
         y(j,h)  passengers actually carried
         b(j,h)  passengers bumped
         oc      operating cost
         bc      bumping cost ;

Variable
        phi     total expected costs;

Equations

         ab(i)   aircraft balance
         db(j)   demand balance
         yd(j,h) definition of boarded passengers
         bd(j,h) definition of bumped passengers
         ocd     operating cost definition
         bcd1    bumping cost definition: version 1
         bcd2    bumping cost definition: version 2
         obj     objective function;

 ab(i)..   sum(j, x(i,j)) =l= aa(i);

 db(j)..   sum(i, p(i,j)*x(i,j)) =g= sum(h$deltb(j,h), y(j,h));

 yd(j,h).. y(j,h) =l= sum(i, p(i,j)*x(i,j));

 bd(j,h).. b(j,h) =e= dd(j,h) - y(j,h);

 ocd..     oc =e= sum((i,j), c(i,j)*x(i,j));

 bcd1..    bc =e= sum(j, k(j)*(ed(j)-sum(h, gamma(j,h)*y(j,h))));

 bcd2..    bc =e= sum((j,h), k(j)*lambda(j,h)*b(j,h));

 obj..     phi =e= oc + bc ;



Model    alloc1 aircraft allocation version 1 / ab, db, ocd, bcd1, obj /
         alloc2 aircraft allocation version 2 / ab, yd, bd, ocd, bcd2, obj / ;



* version 1 requires upper bounds on y

   y.up(j,h) = deltb(j,h)

 Solve alloc1 minimizing phi using MIP; Display y.l;

   y.up(j,h) = +inf;

 Solve alloc2 minimizing phi using MIP; Display y.l;

*output


file outputfile1 / 'Flightplan.txt'/;

put outputfile1;


loop(i,
     loop(j,
         loop(ijr$IJrIJ(ijr,i,j),
             put ijr.tl:0, ' ; ' i.tl:0, ' ; ' j.tl:0, ' ; ', x.l(i,j) /
         );
     );
);
putclose outputfile1;

file outputfile2 / 'passengers.txt'/;
put outputfile2;





loop(j,
     loop(h,
         loop(jhr$JHrJH(jhr,j,h),
             put jhr.tl:0, ' ; ' j.tl:0, ' ; ' h.tl:0, ' ; ', y.l(j,h), ' ; ', b.l(j,h)/
         );
     );
);


putclose outputfile2;

file outputfile3 / 'total_costs.txt'/;

put outputfile3;

put 'total_costs:  ',phi.l /

put '**************************'

putclose outputfile3;

execute_unload 'results.gdx',phi.l,y,b,x;

execute'=gdx2xls results.gdx results.xlsx';































