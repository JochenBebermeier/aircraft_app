Rails.application.routes.draw do

  resources :route_state_associations
  resources :route_plane_associations
  post "periods/change", as: :change_periods


  resources :machine_period_associations
  resources :periods
  resources :product_period_associations
  resources :product_product_associations
  resources :machines
  resources :items
  root             'static_pages#home'
  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]

  resources :translinks
  resources :demandsites
  resources :supplysites
  resources :sites
  resources :products
  resources :states
  resources :planes
  resources :routes


  get 'transport_start', to: 'static_pages#transport_start'
  get 'mlclsp_start', to: 'static_pages#mlclsp_start'
  get 'aircraft_start', to: 'static_pages#aircraft_start'

  post 'translinks/read_and_show_ofv', :to => 'translinks#read_and_show_ofv'
  post 'translinks/read_transportation_quantities', :to => 'translinks#read_transportation_quantities'
  post 'translinks/optimize', :to => 'translinks#optimize'
  post 'translinks/delete_transportation_quantities', :to => 'translinks#delete_transportation_quantities'

  post 'product_periods/read_and_show_ofv', :to => 'product_period_associations#read_and_show_ofv'
  post 'product_periods/read_optimization_results', :to => 'product_period_associations#read_optimization_results'
  post 'product_periods/optimize', :to => 'product_period_associations#optimize'
  post 'product_periods/delete_old_plan', :to => 'product_period_associations#delete_old_plan'
  post 'product_periods/show_index_page', :to => 'product_period_associations#show_index_page'

  post 'route_planes/read_and_show_ofv', :to => 'route_plane_associations#read_and_show_ofv'
  post 'route_planes/read_optimization_results', :to => 'route_plane_associations#read_optimization_results'
  post 'route_states/read_and_show_passengers', :to => 'route_state_associations#read_and_show_passengers'
  post 'route_planes/optimize', :to => 'route_plane_associations#optimize'
  post 'route_planes/delete_old_plan', :to => 'route_plane_associations#delete_old_plan'
  post 'route_planes/show_index_page', :to => 'route_plane_associations#show_index_page'

end
