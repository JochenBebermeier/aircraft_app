require 'rails_helper'
require_relative  '../app/controllers/route_plane_associations_controller'


describe RoutePlaneAssociation do

before do
  @route_plane_association = RoutePlaneAssociation.new(route_id: 1, plane_id: 1, capacity: 1, costs: 1, planes_assigned: 0)
end

  subject { @route_plane_association }

  it { should respond_to(:route_id) }
  it { should respond_to(:plane_id) }
  it { should respond_to(:capacity) }
  it { should respond_to(:costs) }
  it { should respond_to(:planes_assigned) }
  it { should be_valid }

  describe "when route_id is not present" do
    before { @route_plane_association.route_id = " "}
    it { should_not be_valid }
  end

  describe "when plane_id is not present" do
    before { @route_plane_association.plane_id = " "}
    it { should_not be_valid }
  end

 describe "when capacity is not present" do
   before { @route_plane_association.capacity = " "}
   it { should_not be_valid }
 end

  describe "when costs is not present" do
    before { @route_plane_association.costs = " "}
    it { should_not be_valid }
  end


  describe "when capacity is negative" do
    before { @route_plane_association.capacity = -1.0}
    it { should_not be_valid }
  end

  describe "when costs is negative" do
    before { @route_plane_association.costs = -1.0}
    it { should_not be_valid }
  end




end