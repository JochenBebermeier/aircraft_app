require 'rails_helper'
require_relative  '../app/controllers/planes_controller'


describe Plane do

before do
  @plane = Plane.new(name: "A", plane_availability: 10)
end

  subject { @plane }

  it { should respond_to(:name) }
  it { should respond_to(:plane_availability) }

  it { should be_valid }

  describe "when name is not present" do
    before { @plane.name = " "}
    it { should_not be_valid }
  end

  describe "when plane_availability is not present" do
    before { @plane.plane_availability = " "}
    it { should_not be_valid }
  end

describe "when plane_availability is negative" do
  before { @plane.plane_availability = -3.0}
  it { should_not be_valid }
end


end