require 'rails_helper'
require_relative  '../app/controllers/states_controller'


describe State do

before do
  @state = State.new(name: "S1")
end

  subject { @state }

  it { should respond_to(:name) }

  it { should be_valid }

  describe "when name is not present" do
    before { @state.name = " "}
    it { should_not be_valid }
  end

end