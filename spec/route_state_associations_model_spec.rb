require 'rails_helper'
require_relative  '../app/controllers/route_state_associations_controller'


describe RouteStateAssociation do

before do
  @route_state_association = RouteStateAssociation.new(route_id: 1, state_id: 1, demand: 1, lambda: 1, passengers_carried: 0, passengers_bumped: 0)
end

  subject { @route_state_association }

  it { should respond_to(:route_id) }
  it { should respond_to(:state_id) }
  it { should respond_to(:demand) }
  it { should respond_to(:lambda) }
  it { should respond_to(:passengers_carried) }
  it { should respond_to(:passengers_bumped) }
  it { should be_valid }

  describe "when route_id is not present" do
    before { @route_state_association.route_id = " "}
    it { should_not be_valid }
  end

  describe "when state_id is not present" do
    before { @route_state_association.state_id = " "}
    it { should_not be_valid }
  end

 describe "when demand is not present" do
   before { @route_state_association.demand = " "}
   it { should_not be_valid }
 end

  describe "when lambda is not present" do
    before { @route_state_association.lambda = " "}
    it { should_not be_valid }
  end


  describe "when demand is negative" do
    before { @route_state_association.demand = -1.0}
    it { should_not be_valid }
  end

  describe "when lambda is negative" do
    before { @route_state_association.lambda = -1.0}
    it { should_not be_valid }
  end

  describe "when passengers_carried is negative" do
    before { @route_state_association.passengers_carried = -1.0}
    it { should_not be_valid }
  end


end