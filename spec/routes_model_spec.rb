require 'rails_helper'
require_relative  '../app/controllers/routes_controller'


describe Route do

before do
  @route = Route.new(name: "R1", revenue_loss: 10)
end

  subject { @route }

  it { should respond_to(:name) }
  it { should respond_to(:revenue_loss) }

  it { should be_valid }

  describe "when name is not present" do
    before { @route.name = " "}
    it { should_not be_valid }
  end

  describe "when plane_availability is not present" do
    before { @route.revenue_loss = " "}
    it { should_not be_valid }
  end

describe "when revenue_loss is negative" do
  before { @route.revenue_loss = -1.0}
  it { should_not be_valid }
end


end