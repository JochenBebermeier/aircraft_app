set i / 
TypeA
TypeB
TypeC
TypeD
/

set j / 
R1
R2
R3
R4
R5
/

set h / 
Scenario1
Scenario2
Scenario3
Scenario4
Scenario5
/;

set ijr / 
ijr1
ijr2
ijr3
ijr4
ijr5
ijr6
ijr7
ijr8
ijr9
ijr10
ijr11
ijr12
ijr13
ijr14
ijr15
ijr16
ijr17
/;

set jhr / 
jhr1
jhr2
jhr3
jhr4
jhr5
jhr6
jhr7
jhr8
jhr9
jhr10
jhr11
jhr12
jhr13
jhr14
jhr15
jhr16
jhr17
jhr18
jhr19
jhr20
/;

IJ(i,j)=no;
JH(j,h)=no;

IJ('TypeA','R1')=yes;
IJ('TypeB','R1')=yes;
IJ('TypeA','R2')=yes;
IJ('TypeB','R2')=yes;
IJ('TypeC','R2')=yes;
IJ('TypeD','R2')=yes;
IJ('TypeA','R3')=yes;
IJ('TypeB','R3')=yes;
IJ('TypeD','R3')=yes;
IJ('TypeA','R4')=yes;
IJ('TypeB','R4')=yes;
IJ('TypeC','R4')=yes;
IJ('TypeD','R4')=yes;
IJ('TypeA','R5')=yes;
IJ('TypeB','R5')=yes;
IJ('TypeC','R5')=yes;
IJ('TypeD','R5')=yes;
JH('R1','Scenario1')=yes;
JH('R1','Scenario2')=yes;
JH('R1','Scenario3')=yes;
JH('R1','Scenario4')=yes;
JH('R1','Scenario5')=yes;
JH('R2','Scenario1')=yes;
JH('R2','Scenario2')=yes;
JH('R3','Scenario1')=yes;
JH('R3','Scenario2')=yes;
JH('R3','Scenario3')=yes;
JH('R3','Scenario4')=yes;
JH('R3','Scenario5')=yes;
JH('R4','Scenario1')=yes;
JH('R4','Scenario2')=yes;
JH('R4','Scenario3')=yes;
JH('R4','Scenario4')=yes;
JH('R4','Scenario5')=yes;
JH('R5','Scenario1')=yes;
JH('R5','Scenario2')=yes;
JH('R5','Scenario3')=yes;
IJrIJ(ijr,i,j)=no;
JHrJH(jhr,j,h)=no;

IJrIJ('ijr1','TypeA','R1')=yes;
IJrIJ('ijr2','TypeB','R1')=yes;
IJrIJ('ijr3','TypeA','R2')=yes;
IJrIJ('ijr4','TypeB','R2')=yes;
IJrIJ('ijr5','TypeC','R2')=yes;
IJrIJ('ijr6','TypeD','R2')=yes;
IJrIJ('ijr7','TypeA','R3')=yes;
IJrIJ('ijr8','TypeB','R3')=yes;
IJrIJ('ijr9','TypeD','R3')=yes;
IJrIJ('ijr10','TypeA','R4')=yes;
IJrIJ('ijr11','TypeB','R4')=yes;
IJrIJ('ijr12','TypeC','R4')=yes;
IJrIJ('ijr13','TypeD','R4')=yes;
IJrIJ('ijr14','TypeA','R5')=yes;
IJrIJ('ijr15','TypeB','R5')=yes;
IJrIJ('ijr16','TypeC','R5')=yes;
IJrIJ('ijr17','TypeD','R5')=yes;


JHrJH('jhr1','R1','Scenario1')=yes;
JHrJH('jhr2','R1','Scenario2')=yes;
JHrJH('jhr3','R1','Scenario3')=yes;
JHrJH('jhr4','R1','Scenario4')=yes;
JHrJH('jhr5','R1','Scenario5')=yes;
JHrJH('jhr6','R2','Scenario1')=yes;
JHrJH('jhr7','R2','Scenario2')=yes;
JHrJH('jhr8','R3','Scenario1')=yes;
JHrJH('jhr9','R3','Scenario2')=yes;
JHrJH('jhr10','R3','Scenario3')=yes;
JHrJH('jhr11','R3','Scenario4')=yes;
JHrJH('jhr12','R3','Scenario5')=yes;
JHrJH('jhr13','R4','Scenario1')=yes;
JHrJH('jhr14','R4','Scenario2')=yes;
JHrJH('jhr15','R4','Scenario3')=yes;
JHrJH('jhr16','R4','Scenario4')=yes;
JHrJH('jhr17','R4','Scenario5')=yes;
JHrJH('jhr18','R5','Scenario1')=yes;
JHrJH('jhr19','R5','Scenario2')=yes;
JHrJH('jhr20','R5','Scenario3')=yes;


k('R1')= 13.0;
k('R2')= 13.0;
k('R3')= 7.0;
k('R4')= 7.0;
k('R5')= 1.0;
aa('TypeA')= 10;
aa('TypeB')= 19;
aa('TypeC')= 25;
aa('TypeD')= 15;
c('TypeA','R1')= 18.0;
p('TypeA','R1')= 16;
c('TypeB','R1')= 17.0;
p('TypeB','R1')= 9;
c('TypeA','R2')= 21.0;
p('TypeA','R2')= 15;
c('TypeB','R2')= 15.0;
p('TypeB','R2')= 10;
c('TypeC','R2')= 10.0;
p('TypeC','R2')= 5;
c('TypeD','R2')= 16.0;
p('TypeD','R2')= 11;
c('TypeA','R3')= 18.0;
p('TypeA','R3')= 28;
c('TypeB','R3')= 16.0;
p('TypeB','R3')= 14;
c('TypeD','R3')= 17.0;
p('TypeD','R3')= 22;
c('TypeA','R4')= 16.0;
p('TypeA','R4')= 23;
c('TypeB','R4')= 14.0;
p('TypeB','R4')= 15;
c('TypeC','R4')= 9.0;
p('TypeC','R4')= 7;
c('TypeD','R4')= 15.0;
p('TypeD','R4')= 17;
c('TypeA','R5')= 10.0;
p('TypeA','R5')= 81;
c('TypeB','R5')= 9.0;
p('TypeB','R5')= 57;
c('TypeC','R5')= 6.0;
p('TypeC','R5')= 29;
c('TypeD','R5')= 10.0;
p('TypeD','R5')= 55;
dd('R1','Scenario1')= 200;
lambda('R1','Scenario1')= 0.2;
dd('R1','Scenario2')= 220;
lambda('R1','Scenario2')= 0.05;
dd('R1','Scenario3')= 250;
lambda('R1','Scenario3')= 0.35;
dd('R1','Scenario4')= 270;
lambda('R1','Scenario4')= 0.2;
dd('R1','Scenario5')= 300;
lambda('R1','Scenario5')= 0.2;
dd('R2','Scenario1')= 50;
lambda('R2','Scenario1')= 0.3;
dd('R2','Scenario2')= 150;
lambda('R2','Scenario2')= 0.7;
dd('R3','Scenario1')= 140;
lambda('R3','Scenario1')= 0.1;
dd('R3','Scenario2')= 160;
lambda('R3','Scenario2')= 0.2;
dd('R3','Scenario3')= 180;
lambda('R3','Scenario3')= 0.4;
dd('R3','Scenario4')= 200;
lambda('R3','Scenario4')= 0.2;
dd('R3','Scenario5')= 220;
lambda('R3','Scenario5')= 0.1;
dd('R4','Scenario1')= 10;
lambda('R4','Scenario1')= 0.2;
dd('R4','Scenario2')= 50;
lambda('R4','Scenario2')= 0.2;
dd('R4','Scenario3')= 80;
lambda('R4','Scenario3')= 0.3;
dd('R4','Scenario4')= 100;
lambda('R4','Scenario4')= 0.2;
dd('R4','Scenario5')= 340;
lambda('R4','Scenario5')= 0.1;
dd('R5','Scenario1')= 580;
lambda('R5','Scenario1')= 0.1;
dd('R5','Scenario2')= 600;
lambda('R5','Scenario2')= 0.8;
dd('R5','Scenario3')= 620;
lambda('R5','Scenario3')= 0.1;
